import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {
  HomePage({super.key});
  final _formstate =
      GlobalKey<FormState>(); // membuat formstate untuk key pada widget form
  final textcontroller = TextEditingController();
  String data = 'admin';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('validator'),
      ),
      body: ListView(
        children: [
          Container(
            margin: EdgeInsets.all(20),
            child: Form(
              key: _formstate,
              child: Column(
                children: [
                  TextFormField(
                    controller: textcontroller,
                    validator: (value) {
                      if (value == '') {
                        return 'Isi data terlebih dahulu!';
                      } else if (value != data) {
                        return 'data salah';
                      }
                    },
                    decoration: InputDecoration(
                        border: OutlineInputBorder(), label: Text('nama')),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  ElevatedButton(
                      onPressed: () {
                        if (_formstate.currentState!.validate()) {
                          print('benar');
                        } else {
                          print('salah');
                        }
                      },
                      child: Text('submit'))
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
